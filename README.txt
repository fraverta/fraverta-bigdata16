****************************************************************************************
****************************************************************************************
	Trabajo Final "Programación Distribuida Sobre Grandes Volúmenes de Datos"
																		Fernando Raverta
****************************************************************************************

	Las etapas del pipeline han sido implementadas en dos notebook de Zeppelin
	diferentes, uno se encarga de clasificar los tweets imprimiendo los resultados
	en formato parquet. Mientras que el otro, se encarga del clustering y la 
	generación de datos para la visualización utilizando como input el resultado
	del proceso de clasificación. 

	Para correr las etapas del pipeline es necesario que agregue las siguientes dependencias
	en las sesión del interprete spark:
		1) org.json4s:json4s-native_2.11:3.5.0
		2) PATH_TO_THIS_FOLDER/BigData-FRaverta/spark-stemming_2.10-0.1.2.jar

	A continuación se muestran las intrucciones para correr cada etapa:

		-> Clasificación
				1. Abrir con Zeppelin el notebook classifier.json
				2. Setear el INPUT_PATH al directorio dónde se ubique la carpeta BigData-FRaverta
				3. Establezca los parametros del análisis y ejecute.
				4. Los resultados son impresos en formato parquet en 
					 Los parámetros de la clasificación son impresos en el archivo BigData-FRaverta/classifierOutput/classifierOutput/CLASSIFIER-PARAMETERS.txt

		-> Clustering y recolección de datos para la visualización
				1. Abrir con Zeppelin el notebook clustering.json
				2. Setear el INPUT_PATH al directorio dónde se ubique la carpeta BigData-FRaverta
				3. Establezca los parametros del análisis y ejecute.
				4. Al finalizar la corrida, podrá visualizar los resultados corriendo la visualización.
					Los parámetros de esta etapa son impresos en el archivo: BigData-FRaverta/CLUSTERING-PARAMETERS.txt

		-> Ejecutar visualización:	
					python run.py (portNumber)						
					Por defecto el servidor correra en el puerto 8000. 

****************************************************************************************
