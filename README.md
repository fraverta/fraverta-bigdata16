
# Final work "Distributed Programming over Large Volumes of Data"
By Fernando Raverta

The stages of the pipeline have been implemented in two Zeppelin notebooks different, one is in charge of sorting the tweets by printing the results in parquet format. While the other one, is in charge of clustering and generation of data for visualization using the result as input of the classification process. 

A **quick overview** of the project can be found in files **Presentacion.pdf** and **informe.pdf** (both in spanish)

## To run the stages of the pipeline it is necessary to add the following dependencies in the interpreter spark session:

	1) org.json4s:json4s-native_2.11:3.5.0
	2) PATH_TO_THIS_FOLDER/BigData-FRaverta/spark-stemming_2.10-0.1.2.jar

## Below are the instructions for running each stage:

**Classification**

 1. Open with Zeppelin the notebook classifier.json
 2. Set the INPUT_PATH to the directory where the BigData-FRaverta folder is located
 3. Set the parameters of the analysis and execute.
 4. The results are printed in parquet format.
 5. The classification parameters are printed in the file BigData-FRaverta/classifierOutput/classifierOutput/CLASSIFIER-PARAMETERS.txt

**Clustering and data collection for visualization**

 1. Open with Zeppelin the notebook clustering.json 
 2. Set the INPUT_PATH to the directory where the BigData-FRaverta folder is located
 3. Set the parameters of the analysis and execute.
 4. At the end of the run, you can view the results by running the display.
 5. The parameters of this stage are printed in the file: BigData-FRaverta/CLUSTERING-PARAMETERS.txt

**Execute visualization**	

	python run.py (portNumber)						
	By default the server will run on port 8000. 


