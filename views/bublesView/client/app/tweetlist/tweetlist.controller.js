(function() {
  'use strict';

  angular
    .module('bubbleApp.tweetlist')
    .controller('TweetListCtrl', TweetListCtrl);

  TweetListCtrl.$inject = [
    '$scope',
    '$http',
    'Bubbles',
    'Tweets'
  ];

  function TweetListCtrl($scope, $http, Bubbles, Tweets ) {
    var vm = this;
    
    //Folowing lines are to enable a correct and generic cluster select option.
    var cluster = decodeURIComponent(location.search).replace("?","")
    if (!cluster)
      cluster = '0'
    else
      cluster = cluster.slice(0, -1);
    $http.get('data/bubbles.json').then(function(response) { var nCluster = response.data.length;
                                                            $scope.data = {
                                                              model: cluster,
                                                              availableOptions: range(0, nCluster - 1, 1)
                                                             };})



    vm.tweetsIds = [];
    vm.activeTerm = false;
    vm.activeCluster =0;

    
    

    Bubbles.onBubbleClick(onBubbleClick);

    activate();

    function activate() {
      getTweets();
    }

    function getTweets() {
      return Tweets.get().then(function(tweets) {
        vm.tweets = tweets;
      });
    }

    function onBubbleClick(tweet) {
      vm.activeTerm = tweet.term;
      vm.tweetsIds = _.uniq(tweet.tweets);
      $scope.$apply();
    }
    
    function range(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) {
            input.push({id:i, name:'Cluster ' + i});
        }
        return input;
    }
  }

})();