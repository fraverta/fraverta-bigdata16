(function() {
  'use strict';

  angular
    .module('app')
    .filter('timestamp', timestamp);

  function timestamp() {
    return function(timestamp) {
      return new Date(timestamp);
    };
  }
})();
