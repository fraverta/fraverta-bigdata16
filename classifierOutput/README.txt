Classifier parameters 
 	 MIN_DF: 4.0
 	 MAX_DF: 2.147483647E9
 	 STEMMING: false
 	 BINARIZATION: false
 	 FRECUENCY: false
 	 TF_IDF: true
 	 N_GRAMA: (1,3)