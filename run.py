import sys
import os

def get_args(name="default", first='8000'):
    return first

port = get_args(*sys.argv)

if sys.version_info < (3,0):
	os.system("python -m SimpleHTTPServer " + port)
else:
	os.system("python -m http.server " + port)

